# tips for handling changes staging
# https://stackoverflow.com/questions/4185105/ways-to-implement-data-versioning-in-mongodb

# using https://github.com/joke2k/faker
from faker import Faker # pip install Faker
import random
import pymongo # python -m pip install pymongo
import datetime
from time import perf_counter

# BEGIN TIMING
begin = perf_counter()

# GET ALL UNIQUE SERVICE REQUEST NUMBERS
with open("../../srnList.csv") as f:
	reqNumbers = f.read().splitlines()

# GET TOTAL COUNT OF UNIQUE REQUEST NUMBERS
reqNumLength = len(reqNumbers)
print("unique request numbers count is:", reqNumLength)

# CONNECT TO THE DATABASE
client = pymongo.MongoClient("localhost", 27017)
db = client.chicago_requests

# INITIALIZE FAKER GENERATORS
fakeNA = Faker()

print("generating users")
ubegin = perf_counter()
users = []
# MAKING SURE THERE ARE DUPLICATE PHONE NUMBERS WITH DIFFERENT NAME UPVOTING THE SAME INCIDENT
# THE FOLLOWING CODE WILL RETURN 1,000,000 RANDOM NAMES AND ADDRESSES AND 2 GROUPS OF THE SAME 500,000 SERVICE REQUEST NUMBER/PHONE NUMBER PAIRS
for _ in range(2):
	print("started generating 500,000")
	fakeT = Faker()
	fakeT.seed_instance(55347)
	random.seed(34561)
	for _ in range(500000):
		r = random.randint(0, reqNumLength-1)
		fullReqs = list(db.requests_collection.find({ "Service Request Number": reqNumbers[r]}))
		reqs = []
		for re in fullReqs:
			reqSum = { "_id": re['_id'], "Service Request Number": re['Service Request Number'], "Type of Service Request": re['Type of Service Request'], "Creation Date": re['Creation Date'], "Status": re['Status']}
			if 'Completion Date' in re:
				reqSum['Completion Date'] = re['Completion Date']
			if 'Ward' in re:
				reqSum['Ward'] = re['Ward']
			reqs.append(reqSum)
		user = { "name": fakeNA.name(), "address": fakeNA.address(), "phone_number": fakeT.phone_number(), "upvoted_requests": reqs }
		db.requests_collection.update_many({ "Service Request Number": reqNumbers[r] }, { "$inc": { "upvotes": 1 } })
		users.append(user)
	print("finished adding 500,000 users to the list,", datetime.timedelta(seconds=perf_counter()-begin), "has passed since the whole process started")

# BULK INSERT OF USERS RETURNING THE IDS
userIds = db.users_data.insert_many(users).inserted_ids

# CREATE INDICES FOR USERS_DATA COLLECTION
db.users_data.create_index([("name", pymongo.ASCENDING)])
db.users_data.create_index([("phone_number", pymongo.ASCENDING)])
db.users_data.create_index([("upvoted_requests._id", pymongo.ASCENDING)])
db.users_data.create_index([("upvoted_requests.Ward", pymongo.ASCENDING)])

# GET TOTAL COUNT OF THE USERS
userCount = len(userIds)
print("users count is:", userCount)
print("creating users with at least 1 upvoted request ended in:", datetime.timedelta(seconds=perf_counter()-ubegin))

updbegin = perf_counter()

# RESEEDING RANDOM INT GENERATOR
random.seed(perf_counter())

# GENERATE 3,000,000 MORE RANDOM UPVOTES
for _ in range(3000000):
	r = random.randint(0, reqNumLength-1)
	u = random.randint(0, userCount-1)

	curUser = db.users_data.find_one({ "_id": userIds[u] })
	exists = 0
	if "upvoted_requests" in curUser:
		if len(curUser["upvoted_requests"]) < 1000:
			for request in curUser["upvoted_requests"]:
				if request["Service Request Number"] == reqNumbers[r]:
					exists = 1
	if exists == 0:
		fullReqs = list(db.requests_collection.find({ "Service Request Number": reqNumbers[r]}))
		reqs = []
		for re in fullReqs:
			reqSum = { "_id": re['_id'], "Service Request Number": re['Service Request Number'], "Type of Service Request": re['Type of Service Request'], "Creation Date": re['Creation Date'], "Status": re['Status']}
			if 'Completion Date' in re:
				reqSum['Completion Date'] = re['Completion Date']
			if 'Ward' in re:
				reqSum['Ward'] = re['Ward']
			reqs.append(reqSum)
		db.users_data.update_one( { "_id": userIds[u] }, { "$push": { "upvoted_requests": { "$each": reqs } } })
		db.requests_collection.update_many({ "Service Request Number": reqNumbers[r] }, { "$inc": { "upvotes": 1 } })

print("generating upvotes finished in:", datetime.timedelta(seconds=perf_counter()-updbegin))

client.close()

print("total process runtime:", datetime.timedelta(seconds=perf_counter()-begin))
