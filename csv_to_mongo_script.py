import csv
import json
import pymongo # python -m pip install pymongo
import ast
import datetime
from time import perf_counter

# PROCESS START TIME
begin = perf_counter()

# CSV PATHS LIST
fileList = [
		"../../../proj1/data/311-service-requests-abandoned-vehicles.csv",
		"../../../proj1/data/311-service-requests-alley-lights-out.csv",
		"../../../proj1/data/311-service-requests-garbage-carts.csv",
		"../../../proj1/data/311-service-requests-graffiti-removal.csv",
		"../../../proj1/data/311-service-requests-pot-holes-reported.csv",
		"../../../proj1/data/311-service-requests-rodent-baiting.csv",
		"../../../proj1/data/311-service-requests-sanitation-code-complaints.csv",
		"../../../proj1/data/311-service-requests-street-lights-all-out.csv",
		"../../../proj1/data/311-service-requests-street-lights-one-out.csv",
		"../../../proj1/data/311-service-requests-tree-debris.csv",
		"../../../proj1/data/311-service-requests-tree-trims.csv"
	]

# CONNECT TO THE DATABASE
client = pymongo.MongoClient("localhost", 27017)
db = client.chicago_requests

for f in fileList:
	fbegin = perf_counter()
	curCsv = open(f, "r")
	curFile = csv.DictReader(curCsv)

	# CREATE AN ARRAY OF ROWS
	rows = []
	for row in curFile:
		if row["Latitude"] and row["Longitude"]:
			gp = { "type": "Point", "coordinates": [float(row["Longitude"]), float(row["Latitude"])] }
			row["GeoPoint"] = gp

		for fld in curFile.fieldnames:
			if not row[fld] or (fld == "Latitude") or (fld == "Longitude"):
				row.pop(fld)
			# CONVERTING DATES TO DATE FORMAT
			elif fld == "Creation Date":
				cr = datetime.datetime.strptime(row[fld], "%Y-%m-%dT%H:%M:%S")
				row[fld] = cr
			elif fld == "Completion Date":
				co = datetime.datetime.strptime(row[fld], "%Y-%m-%dT%H:%M:%S")
				row[fld] = co
			# READING LOCATION FIELD AS JSON OBJECT
			elif (fld == "Location") and row[fld]:
				di = ast.literal_eval(row[fld])
				try:
					float(di['latitude'])
				except:
					ValueError
				else:
					di['latitude'] = float(di['latitude'])
				try:
					float(di['longitude'])
				except:
					ValueError
				else:
					di['longitude'] = float(di['longitude'])
				# REPLACING LOCATION STRING WITH JSON OBJECT
				row[fld] = di
			# DETECT NUMERIC FIELDS
			elif row[fld].isnumeric():
				row[fld] = int(row[fld])
			elif row[fld].find(".") != -1:
				try:
					float(row[fld])
				except:
					ValueError
				else:
					row[fld] = float(row[fld])

		rows.append(row)

		# PRETTY PRINT OF ROW
		# print(json.dumps(row, indent=2))

	# CREATE DB AND COLLECTION IF THEY DONT ALREADY EXIST AND ADD THE ROWS
	db.requests_collection.insert_many(rows)
	# CLOSE THE CSV FILE
	curCsv.close()

	fend = perf_counter()
	print("finished with", f)
	print("runtime interval:", datetime.timedelta(seconds=fend-fbegin))

ibegin = perf_counter()
print("creating indices")
db.requests_collection.create_index([("Service Request Number", pymongo.ASCENDING)])
db.requests_collection.create_index([("Type of Service Request", pymongo.ASCENDING)])
db.requests_collection.create_index([("ZIP Code", pymongo.ASCENDING)])
db.requests_collection.create_index([("Ward", pymongo.ASCENDING)])
db.requests_collection.create_index([("Creation Date", pymongo.ASCENDING)])
db.requests_collection.create_index([("GeoPoint", "2dsphere")])
iend = perf_counter()
print("finished creating indices")
print("runtime interval:", datetime.timedelta(seconds=iend-ibegin))

client.close()
end = perf_counter()

print("end of process")
print("total runtime:", datetime.timedelta(seconds=end-begin))
