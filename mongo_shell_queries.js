/*
Find the total requests per type that were created within a specified time range and sort
them in a descending order.
*/
db.requests_collection.aggregate([
	{
		$match: {
			"Creation Date": { $gte: ISODate("2016-01-01"), $lte: ISODate("2016-07-01") }
		}
	},
	{
		$sortByCount: "$Type of Service Request"
	}
])

/*
Find the number of total requests per day for a specific request type and time range.
*/
db.requests_collection.aggregate([
	{
		$match: {
			"Type of Service Request": "Abandoned Vehicle Complaint",
			"Creation Date": { $exists: 1, $gte: ISODate("2016-01-01"), $lte: ISODate("2016-07-01") }
		}
	},
	{
		$group: {
			_id: "$Creation Date",
			requests_count: {$sum: 1}
		}
	},
	{
		$sort : {"_id": 1}
	}
])

/* avg requests for entire date range -- probably not needed */
// db.requests_collection.aggregate([
// 	{
// 		$match: {
// 			"Type of Service Request": "Abandoned Vehicle Complaint",
// 			"Creation Date": { $exists: 1, $gte: ISODate("2016-01-01"), $lte: ISODate("2016-07-01") }
// 		}
// 	},
// 	{
// 		$count: "requests_count"
// 	},
// 	{
// 		$project: {
// 			"Type of Service Request": 1,
// 			reqsPerDay: {$divide: ["$requests_count", {$divide: [ {$subtract: [ISODate("2016-07-01"), ISODate("2016-01-01")]}, 86400000]}]}
// 		}
// 	}
// ])

/*
Find the three most common service requests per zipcode for a specific day.
*/
db.requests_collection.aggregate([
	{
		$match: {
			"Creation Date": ISODate("2016-02-01"),
			"ZIP Code": { $exists: 1, $gt: 0 }
		}
	},
	{
		$group: {
			_id: {zipCode: "$ZIP Code", type: "$Type of Service Request"},
			count: {$sum: 1},
		}
	},
	{
		$sort: {
			"_id.zipCode": 1,
			"count": -1
		}
	},
	{
		$group: {
			_id: { zc: "$_id.zipCode" },
			types: {$push: {type: "$_id.type", count: "$count"}}
		}
	},
	{
		$project: {
			types: {$slice: ["$types", 3]}
		}
	},
	{
		$sort: {
			"_id.zc" : 1
		}
	}
])

/*
Find the three least common wards with regards to a given service request type.
*/
db.requests_collection.aggregate([
	{
		$match: {
			"Type of Service Request": "Abandoned Vehicle Complaint",
			"Ward": { $exists: 1, $gt: 0 }
		}
	},
	{
		$group: {
			_id: {ward: "$Ward"},
			count: {$sum: 1}
		}
	},
	{
		$sort: { "count": 1 }
	},
	{ $limit: 3 },
	{
		$project: {
			_id: 0,
			ward: "$_id.ward",
			count: "$count"
		}
	}
])

/*
Find the average completion time per service request for a specific date range.
*/
db.requests_collection.aggregate([
    { $match : { "Creation Date": { $exists: 1, $gte: ISODate("2016-01-01"), $lte: ISODate("2016-07-01") }, "Completion Date": { $exists: 1 } } },
    { $group : { _id : null, avg_days: { $avg: { $divide: [ { $subtract: ["$Completion Date", "$Creation Date"] }, 86400000] } } } },
    { $project : { _id: 0, avg_days: 1 } } ])

/*
Find the most common service request in a specified bounding box for a specific day. You
are encouraged to use GeoJSON objects and Geospatial Query Operators.
*/
db.requests_collection.aggregate([
	{
		$match: {
			"Creation Date": ISODate("2016-02-01"),
			"GeoPoint": {
				$geoWithin : {
					$geometry: {
						type: "Polygon",
						coordinates: [[ [-87.7055101 , 41.8867294], [-87.659202, 41.909212], [-87.6607461, 41.8939314], [-87.678035, 41.895063], [-87.7055101 , 41.8867294] ]]
					}
				}
			}
		}
	},
	{
		$group : {
			_id: "$Type of Service Request",
			count: {$sum: 1}
		}
	},
	{
		$project : {
			_id: 0,
			requestType: "$_id",
			count: {$max : "$count"}
		}
	},
	{
		$sort : {
			"count": -1
		}
	},
	{
		$limit : 1
	}
])

/*
	instead of $geometry could use $box but it works for flat surfaces:
	[[<bottom left coords>], [<upper right coords>]]
	$box: [ [-87.678035, 41.895063], [-87.659202, 41.909212] ]
*/

/*
Find the fifty most upvoted service requests for a specific day.
*/
db.requests_collection.aggregate([
	{
		$match : {
			"Creation Date": ISODate("2016-01-01"),
			"upvotes": { $exists: 1 }
		}
	},
	{
		$project : {
			_id : 0,
			"Service Request Number" : 1,
			"upvotes" : 1
		}
	},
	{
		$sort : {
			"upvotes" : -1
		}
	},
	{
		$limit : 50
	}
])


/*
Find the fifty most active citizens, with regard to the total number of upvotes.
*/
db.users_data.aggregate([
    {
        $project : {
            _id : 0,
            "name" : 1,
            "address" : 1,
            "phone_number" : 1,
            numberOfRequests : { $cond: { if: { $isArray: "$upvoted_requests" }, then: { $size: "$upvoted_requests" }, else: 0 } }
        }
    },
    {
        $sort : {
            "numberOfRequests" : -1
        }
    },
    {
        $limit : 50
    }
])

/*
Find the top fifty citizens, with regard to the total number of wards for which they have
upvoted an incidents
*/
db.users_data.aggregate([
	{
		$project : {
			wards : "$upvoted_requests.Ward"
		}
	},
	{
		$unwind : "$wards"
	},
	{
        $group : {
                _id : { usrID: "$_id", ward: "$wards"}
        }
    },
    {
    	$sortByCount : "$_id.usrID"
    },
    {
    	$limit : 50
    }
],
{
	allowDiskUse : true
})

/*
Find all incident ids for which the same telephone number has been used for more than one
names.
*/
db.users_data.aggregate([
	{
		$project : {
			"name" : 1,
			"phone_number" : 1,
			incidents : "$upvoted_requests._id"
		}
	},
	{
		$unwind : "$incidents"
	},
	{
		$group : {
			_id : { phoneNumber: "$phone_number", incidentID: "$incidents" },
			names : { $addToSet: "$name" }
		}
	},
	{
		$project : {
			names : "$names",
			count : { $size: "$names" }
		}
	},
	{
		$match : {
			"count" : { $gt: 1 }
		}
	},
	{
		$sort : {
			"count" : -1
		}
	}
],
{
	allowDiskUse: true
}).pretty()

/*
	if the corresponding view was created
*/
db.incidents_per_phone_number.aggregate([
	{
		$match : {
			"count" : { $gt: 1 }
		}
	}
],
{
	allowDiskUse: true
})

/*
Find all the wards in which a given name has casted a vote for an incident taking place in it.
*/
db.users_data.aggregate([
	{
		$match : {
			"name" : "Jasmine Mitchell"
		}
	},
	{
		$project : {
			_id: 0,
			wards : "$upvoted_requests.Ward"
		}
	},
	{
		$unwind : "$wards"
	},
	{
		$sortByCount: "$wards"
	},
	{
		$project : {
			_id : 0,
			ward : "$_id",
			votesCount: "$count"
		}
	}
]).pretty()

/*
alternative query sorted by ward:
db.users_data.aggregate([
	{
		$match : {
			"name" : "Jasmine Mitchell"
		}
	},
	{
		$project : {
			_id: 0,
			wards : "$upvoted_requests.Ward"
		}
	},
	{
		$unwind : "$wards"
	},
	{
		$group: {
			_id : { ward: "$wards" },
			upvotesCount: { $sum: 1 }
		}
	},
	{
		$project: {
			_id : 0,
			ward : "$_id.ward",
			upvotesCount: "$upvotesCount"
		}
	},
	{
		$sort : {
			"ward" : 1
		}
	}
]).pretty()

*/
