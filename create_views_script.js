// VIEW FOR GROUPING USER VOTES PER PHONE NUMBER AND INCIDENT ID
db.createView(
	"incidents_per_phone_number",
	"users_data",
	[
		{
			$project : {
				"name" : 1,
				"phone_number" : 1,
				incidents : "$upvoted_requests._id"
			}
		},
		{
			$unwind : "$incidents"
		},
		{
			$group : {
				_id : { phoneNumber: "$phone_number", incidentID: "$incidents" },
				names : { $addToSet: "$name" }
			}
		},
		{
			$project : {
				names : "$names",
				count : { $size: "$names" }
			}
		},
		{
			$sort : {
				"count" : -1
			}
		}
	]
)
